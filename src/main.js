import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import pinia from '@/stores/index'
import 'jquery/dist/jquery.min'
import 'bootstrap/dist/css/bootstrap.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import 'element-plus/theme-chalk/el-notification.css'
import 'element-plus/theme-chalk/el-message.css'

const app = createApp(App)
// app.use(store).use(router).use(ElementPlus).use(ElementPlusIconsVue)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
//下面的导入了自己的属性 尽可能的往下放 保证其权重
//要支持scss需要安装预处理器 pnpm add sass -D
import '@/assets/main.scss'

// const app = createApp(App)

app.use(pinia)
app.use(router)

app.mount('#app')
