import { createRouter, createWebHistory } from 'vue-router'
import { useUserStore } from '@/stores'

//import.meta.env.BASE_URL这是vite中的环境变量可以在vite.config.js中配置 base: '/jd',
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  //往routes里面配路由规则 异步组件的写法 单词可能拼写错误 组件加载不出来记得检查
  routes: [
    //404
    {
      path: '/404',
      name: '404',
      component: () => import('@/views/user/404.vue')
      // component: () => import('@/views/404.html')
    },
    //登录
    {
      path: '/login',
      name: 'login',
      component: () => import('@/views/login/LoginPage.vue')
    },
    //架子
    {
      path: '/',
      name: 'layout',
      component: () => import('@/views/layout/LayoutContainer.vue'),
      redirect: '/home',
      children: [
        // 首页 -- 控制台
        {
          path: '/home',
          name: 'home',
          meta: {
            label: '首页',
            icon: 'House'
          },
          component: () => import('@/views/HomePage.vue')
        },
        //统计管理
        {
          path: '/count',
          name: 'count',
          meta: {
            label: '统计管理'
          },
          component: () => import('@/views/count/CountManage.vue')
        },
        // 员工管理
        {
          path: '/employee',
          name: 'empoyee',
          meta: {
            label: '员工管理',
            icon: 'UserFilled'
          },
          children: [
            {
              path: 'profile',
              name: 'empProfile',
              meta: {
                label: '基本资料',
                icon: 'User'
              },
              component: () => import('@/views/employee/EmployeeProfile.vue')
            },
            {
              path: 'manage',
              name: 'profileManage',
              meta: {
                label: '员工管理',
                icon: 'Edit'
              },
              component: () => import('@/views/employee/EmployeeManage.vue')
            }
          ]
        },
        //
        //人事管理
        {
          path: '/personnel',
          name: 'personnel',
          meta: {
            label: '人事管理'
          },
          children: [
            {
              path: 'assessment',
              meta: {
                label: '员工考核'
              },
              component: () => import('@/views/personnel/StaffAssessment.vue')
            },
            {
              path: 'salary',
              meta: {
                label: '员工调薪'
              },
              component: () => import('@/views/personnel/StaffSalary.vue')
            },
            {
              path: 'transfer',
              meta: {
                label: '员工调职',
                icon: ''
              },
              component: () => import('@/views/personnel/StaffTransfer.vue')
            },
            {
              path: 'reward',
              meta: {
                label: '员工奖惩',
                icon: ''
              },
              component: () => import('@/views/personnel/StaffReward.vue')
            },
            {
              path: 'train',
              meta: {
                label: '员工培训',
                icon: ''
              },
              component: () => import('@/views/personnel/StaffTrain.vue')
            }
          ]
        },
        //薪资管理
        {
          path: '/salary',
          name: 'salary',
          meta: {
            label: '薪资管理'
          },
          children: [
            {
              path: '/salary/manage',
              meta: {
                label: '薪资套账管理',
                icon: ''
              },
              component: () => import('@/views/salary/SalaryManage.vue')
            },
            {
              path: '/salary/table',
              meta: {
                label: '工资表',
                icon: ''
              },
              component: () => import('@/views/salary/SalaryTable.vue')
            }
          ]
        },
        //部门管理
        {
          path: '/department',
          name: 'depatment',
          meta: {
            label: '部门管理'
          },
          children: [
            {
              path: '/department/manage',
              meta: {
                label: '部门信息管理'
              },
              component: () => import('@/views/department/DepartManage.vue')
            }
          ]
        },
        //信息管理
        {
          path: '/basic',
          name: 'basic',
          meta: {
            label: '信息管理'
          },
          children: [
            {
              path: '/basic/position',
              meta: {
                label: '职位管理'
              },
              component: () => import('@/views/basic/PositionManage.vue')
            },
            {
              path: '/basic/joblevel',
              meta: {
                label: '职称管理'
              },
              component: () => import('@/views/basic/JobLevelManage.vue')
            }
          ]
        },
        //系统管理
        {
          path: '/system',
          name: 'system',
          meta: {
            label: '系统管理'
          },
          children: [
            {
              path: '/system/menu',
              meta: {
                label: '菜单管理',
                icon: ''
              },
              component: () => import('@/views/system/SystemMenu.vue')
            },
            {
              path: '/system/port',
              meta: {
                label: '接口管理',
                icon: ''
              },
              component: () => import('@/views/system/SystemPort.vue')
            },
            {
              path: '/system/user',
              meta: {
                label: '用户管理',
                icon: ''
              },
              component: () => import('@/views/system/SystemUser.vue')
            },
            {
              path: '/system/character',
              meta: {
                label: '角色管理',
                icon: ''
              },
              component: () => import('@/views/system/SystemCharacter.vue')
            },
            {
              path: '/system/log',
              meta: {
                label: '日志管理',
                icon: ''
              },
              component: () => import('@/views/system/SystemLog.vue')
            }
          ]
        },

        //user个人中心
        {
          path: '/user',
          name: 'user',
          meta: {
            label: '个人中心'
          },
          children: [
            {
              path: '/user/password',
              meta: {
                label: '修改密码',
                icon: ''
              },
              component: () => import('@/views/user/UserPassword.vue')
            },
            {
              path: '/user/profile',
              meta: {
                label: '基本资料',
                icon: ''
              },
              component: () => import('@/views/user/UserProfile.vue')
            },
            {
              path: '/user/avatar',
              meta: {
                label: '更换头像',
                icon: ''
              },
              component: () => import('@/views/user/UserAvatar.vue')
            }
          ]
        }
      ]
    }
  ]
})

//登录访问拦截
router.beforeEach((to) => {
  const useStore = useUserStore()
  //没有token并且访问的不是登录页就给跳转到登录页
  if (!useStore.token && to.path !== '/login') return '/login'
})
export default router
