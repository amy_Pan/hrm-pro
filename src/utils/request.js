import axios from 'axios'
import { useUserStore } from '@/stores'
import { ElMessage } from 'element-plus'
import router from '@/router'
const baseURL = '/api' // http://175.178.103.19:9966/  => /api

const instance = axios.create({
  // TODO 1. 基础地址，超时时间
  baseURL,
  timeout: 10000
})

//请求拦截器
instance.interceptors.request.use(
  (config) => {
    // TODO 2. 携带token
    const useStore = useUserStore()
    if (useStore.token) {
      config.headers.token = useStore.token
    }
    return config
  },
  (err) => Promise.reject(err)
)

//响应拦截器 待完善
instance.interceptors.response.use(
  (res) => {
    // TODO 3. 处理业务失败
    // TODO 4. 摘取核心响应数据
    //成功返回res
    if (res.data.code === 2000) {
      return res
    } else if (res.data.code === 3401 || res.data.code === 4301) {
      const useStore = useUserStore()

      useStore.removeToken()
      useStore.setUser({})
      router.push('/login')
    } else if (res.data.code === 404) {
      router.push('/404')
    }
    //业务失败 抛出错误
    ElMessage.error(res.data.message || '服务异常')
    return Promise.reject(res.data)
  },
  // TODO 5. 处理401错误(未登录 返回登录页面 不过这里好像没啥用 直接在router下面拦截了)
  (err) => {
    if (err.response.status === 3401) {
      router.push('/login')
    }
    console.log('错误在这里')
    //错误的默认情况  这里得根据接口改
    ElMessage.error(err.response.data.message || '服务异常')

    return Promise.reject(err)
  }
)

export default instance
export { baseURL }
