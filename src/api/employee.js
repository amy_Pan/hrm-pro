//获取员工数据
import request from '@/utils/request'
// export const empGetProfileService = (eid) => {
//   return request.get('/employees', {
//     params: { eid }
//   })
// }

// export const empGetProfileService = () => {
//   return request.get('/positions')
// }

//分页查询员工数据
export const empGetProfileService = (currentPage, pageSize, data) => {
  const url =
    '/employees/condition' +
    '?currentPage=' +
    currentPage +
    '&pageSize=' +
    pageSize
  return request.post(url, data)
}
//删除
export const empDelProfileService = (eids) => {
  var url = '/employees/batch/' + eids
  request.delete(url)
}

//根据id查询员工数据
export const empGetOneProfileService = (eid) => {
  var url = '/employees/' + eid
  request.get(url)
}
//新增
export const empAddProfileService = (data) => {
  request.post('/employees', data)
}

//获取民族列表
export const empGetNationService = () => {
  return request.get('/nations')
}
//获取政治面貌
export const empGetPoliticalService = () => request.get('/politicalFaces')
//修改员工数据
export const empEditProfileService = (data) => {
  request.put('/employees', data)
}
