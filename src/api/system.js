import request from '@/utils/request'
//分页查询日志
export const sysGetLogService = (currentPage, pageSize) => {
  const url =
    '/system/operations' +
    '/?currentPage=' +
    currentPage +
    '&pageSize=' +
    pageSize
  return request.get(url)
}
//删除日志
export const sysDelLogService = (ids) => {
  var url = '/system/operations/' + ids
  return request.delete(url)
}
//用户管理
//分页查询用户
export const sysGetUserService = (currentPage, pageSize, data) => {
  const url =
    '/system/users/condition' +
    '/?currentPage=' +
    currentPage +
    '&pageSize=' +
    pageSize
  return request.post(url, data)
}
//添加
export const sysAddUserService = (data) => request.post('/system/users', data)
//删除
export const sysDelUserService = (userIds) => {
  var url = '/system/users/' + userIds
  request.delete(url)
}
//修改
export const sysEditUserService = (data) => request.put('/system/users', data)
//角色管理
//分页查询角色信息
export const sysGetCharacterService = (currentPage, pageSize, data) => {
  const url =
    '/system/roles/condition' +
    '/?currentPage=' +
    currentPage +
    '&pageSize=' +
    pageSize
  return request.post(url, data)
}
//添加
export const sysAddCharacterService = (data) => {
  request.post('/system/roles', data)
}
//删除
export const sysDelCharacterService = (roleIds) => {
  var url = '/system/roles/' + roleIds
  request.delete(url)
}
//更新
export const sysEditCharacterService = (data) =>
  request.put('/system/roles', data)
//菜单管理
//获取菜单列表 把data删掉才可以查得出
export const sysGetMenuService = (currentPage, pageSize, data) => {
  const url =
    '/system/menus/condition' +
    '/?currentPage=' +
    currentPage +
    '&pageSize=' +
    pageSize
  return request.post(url, data)
}
//添加
export const sysAddMenuService = (data) => request.post('/system/menus', data)
//删除
export const sysDelMenuService = (menuIds) => {
  var url = '/system/menus/' + menuIds
  request.delete(url)
}
//更新
export const sysEditMenuService = (data) => request.put('/system/menus', data)
