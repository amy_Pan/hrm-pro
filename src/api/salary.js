import request from '@/utils/request'
//工资表
export const salGetSalaryService = (currentPage, pageSize, data) => {
  const url =
    '/salarys/extends/condition' +
    '?currentPage=' +
    currentPage +
    '&pageSize=' +
    pageSize
  return request.post(url, data)
}
//薪资套账
//获取薪资套账
export const salGetSalarysService = () => request.get('/salarys')
//添加薪资套账
export const salAddSalaryService = (data) => request.post('/salarys', data)
//更新薪资套账
export const salEditSalaryService = (data) => request.put('/salarys', data)
//删除薪资套账
export const salDelSalaryService = (sids) => {
  var url = '/salarys/' + sids
  request.delete(url)
}
