import request from '@/utils/request'
//获取部门信息
export const depGetTreeService = () => request.get('/departments')
//新增部门
export const depAddDepartmentService = (data) =>
  request.post('/departments', data)
//删除部门
export const depDelDepartmentService = (depId) => {
  var url = '/departments/' + depId
  request.delete(url, depId)
}
//更新部门
export const depEditDepartmentService = (data) =>
  request.put('/departments', data)
