import request from '@/utils/request'

//登录接口
export const userLoginService = ({ password, username }) => {
  return request.post('/login', { password, username })
}

//获取用户基本信息
export const userGetInfoService = () => request.get('/commons/userInfo')
//获取用户菜单树
export const userGetTreeService = () => request.get('/commons/menuInfo')

//测试 获取员工民族
export const employeeGetNations = () => request.get('/nations')

//更改用户密码
export const userGetPwdService = ({ newPass, oldPass }) =>
  request.put('/commons/modifyPassword', { newPass, oldPass })
//更改头像
export const userUpAvatarService = (img) =>
  request.post('/commons/upload/avatar', img)
//更改用户信息
export const userEditProfieService = (data) =>
  request.put('/commons/userInfo/modify', data)

export const getCode = () => {
  return request.get('/captchaImage')
}
