import request from '@/utils/request'
//获取全部职位信息
export const basicGetPositionService = () => request.get('/positions')
//新增
export const basicAddPositionService = (data) =>
  request.post('/positions', data)
//修改
export const basicEditPositionService = (data) =>
  request.put('/positions', data)
//删除
export const basicDelPositionService = (posIds) => {
  var url = '/positions/' + posIds
  request.delete(url)
}
//获取全部职称信息
export const basicGetJoblevelService = () => request.get('/jobLevels')
//新增
export const basicAddJoblevelService = (data) =>
  request.post('/jobLevels', data)
//删除
export const basicDelJoblevelService = (jobLevelIds) => {
  var url = '/jobLevels/' + jobLevelIds
  request.delete(url)
}
//修改
export const basicEditJoblevelService = (data) =>
  request.put('/jobLevels', data)
