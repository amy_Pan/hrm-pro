import request from '@/utils/request'

// 这个是考核结果接口的前缀，固定的
const personnels = '/personnels/appraises'

//根据考核id获取考核结果

// GET /api/personnels/appraises/1  todo 这个为Path请求方式 这个就是restful风格请求方式

// /personnels/appraises/{aid} 后台会自动识别 aid = 1
// GET /api/personnels/appraises?aid=1 todo 这个为param请求方式

export const perGetAssessmentService = (aid) => {
  // aid前面那个斜杆不能少
  var url = '/personnels/appraises/' + aid
  return request.get(url)
  // return request.get('/personnels/appraises', {
  //   params: { aid }
  // })
}

//获取考核结果枚举

export const perGetlevelService = () =>
  request.get('/personnels/appraises/level')

// http://175.178.103.19:9966/personnels/appraises/condition?currentPage=1&pageSize=10
/**
 * post分页 获取考核结果
 *
 * @param {*} currentPage 当前页
 * @param {*} pageSize 每页条数
 * @param {*} data  查询条件 -- 可以不传
 * @returns 分页结果
 */
// export const perGetResultService = (currentPage, pageSize, data) =>
//   request.post('/personnels/appraises/condition', {
//     params: {
//       currentPage,
//       pageSize
//     },
//     data: data
//   })
// export const perGetResultService = (currentPage, pageSize) =>
//   request.post('/personnels/appraises/condition', currentPage, pageSize)

export const perGetResultService = (currentPage, pageSize, data) => {
  const url =
    personnels +
    '/condition' +
    '?currentPage=' +
    currentPage +
    '&pageSize=' +
    pageSize
  return request.post(url, data)
}

//添加
export const perAddAssessmentService = (workId, data) => {
  var url = '/personnels/appraises/save/' + workId
  return request.post(url, data)
}
//编辑
export const perEditAssessmentService = (data) =>
  request.put('/personnels/appraises', data)
//删除
export const perDelAssessmentService = (aids) => {
  var url = '/personnels/appraises/' + aids
  request.delete(url)
}
//员工调薪
//分页获取调薪记录
export const perGetSalaryService = (currentPage, pageSize, data) => {
  const url =
    '/personnels/adjustSalarys/condition' +
    '?currentPage=' +
    currentPage +
    '&pageSize=' +
    pageSize
  return request.post(url, data)
}
//删除
export const perDelSalaryService = (asids) => {
  var url = '/personnels/adjustSalarys/' + asids
  request.delete(url)
}
//添加
export const perAddSalaryService = (workId, data) => {
  var url = '/personnels/adjustSalarys/save/' + workId
  return request.post(url, data)
}
//员工调动
//分页获取调动记录
export const perGetTransferService = (currentPage, pageSize, data) => {
  const url =
    '/personnels/redeployments/condition' +
    '?currentPage=' +
    currentPage +
    '&pageSize=' +
    pageSize
  return request.post(url, data)
}
//删除
export const perDelTransferService = (rids) => {
  var url = '/personnels/redeployments/' + rids
  request.delete(url)
}
//添加
export const perAddTransferService = (workId, data) => {
  var url = '/personnels/redeployments/save/' + workId
  return request.post(url, data)
}
//员工奖惩
//查询
export const perGetRewardService = (currentPage, pageSize, data) => {
  const url =
    '/personnels/rewardAndPunishments/condition' +
    '?currentPage=' +
    currentPage +
    '&pageSize=' +
    pageSize
  return request.post(url, data)
}
//删除
export const perDelRewardService = (rids) => {
  var url = '/personnels/rewardAndPunishments/' + rids
  request.delete(url)
}
//添加
export const perAddRewardService = (workId, data) => {
  var url = '/personnels/rewardAndPunishments/save/' + workId
  return request.post(url, data)
}
//员工培训
//查询
export const perGetTrainService = (currentPage, pageSize, data) => {
  const url =
    '/personnels/trains/condition' +
    '?currentPage=' +
    currentPage +
    '&pageSize=' +
    pageSize
  return request.post(url, data)
}
//删除
export const perDelTrainService = (rids) => {
  var url = '/personnels/trains/' + rids
  request.delete(url)
}
//添加
export const perAddTrainService = (workId, data) => {
  var url = '/personnels/trains/save/' + workId
  return request.post(url, data)
}
