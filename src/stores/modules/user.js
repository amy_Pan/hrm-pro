//导入一个定义模块的方法defineStore
import { defineStore } from 'pinia'
import { ref } from 'vue'
import { userGetInfoService, userGetTreeService } from '@/api/user.js'

// 用户模块 登录token 退出setToken 本身removeToken
//命名 use+仓库名+Store
// export const useUserStore = defineStore(
//   'big-user',
//   () => {
//     const token = ref('') // 定义 token数据
//     const setToken = (t) => (token.value = t) // 设置 token方法

//     return { token, setToken }
//   },
//   {
//     persist: true // 持久化
//   }
// )

//代码
export const useUserStore = defineStore(
  'big-user',
  () => {
    const token = ref('')
    const setToken = (newToken) => {
      token.value = newToken
    }
    const removeToken = () => {
      token.value = ''
    }
    //个人中心需要获取用户数据
    const user = ref({})
    const getUser = async () => {
      const res = await userGetInfoService()
      user.value = res.data.data
    }
    //传对象进行重置功能
    const setUser = (obj) => {
      user.value = obj
    }

    //个人中心用户菜单树
    const tree = ref({})
    const getTree = async () => {
      const res = await userGetTreeService()
      tree.value = res.data.data
    }
    //测试 获取用户民族
    // const nations = ref({})
    // const getNations = async () => {
    //   const res = await employeeGetNations()
    //   nations.value = res.data.data
    // }
    //获取编辑列表
    // const profileList = ref({})
    // const getProfileList = () => {
    //   const str = {}
    //   console.log(str)
    // }
    //暴露
    return {
      token,
      setToken,
      removeToken,
      user,
      getUser,
      tree,
      getTree,
      setUser
    }
  },
  {
    persist: true
  }
)

export const useStoreRowObject = defineStore(
  // 'row',
  // () => {
  //   const getProfileList = () => {
  //     const str = 10
  //     console.log(str)
  //   }

  //   return {
  //     getProfileList
  //   }
  // },

  // {
  //   persist: true
  // }
  'row',
  {
    state: () => ({
      count: 11,
      row: {}
    }),
    getters: {},
    actions: {}
  }
)

// export default useStoreRowObject
