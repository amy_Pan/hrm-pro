import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'

// https://vitejs.dev/config/
export default defineConfig({
    publicPath: './',
    plugins: [
        vue(),
        AutoImport({
            resolvers: [ElementPlusResolver()]
        }),
        Components({
            resolvers: [ElementPlusResolver()]
        })
    ],
    base: '/',
    resolve: {
        alias: {
            '@': fileURLToPath(new URL('./src', import.meta.url))
        }
    },

    //下面这个为了解决跨域后来加的
    server: {
        proxy: {
            '/api': {
                // target: 'http://175.178.103.19:9966/', // http://http://175.178.103.19:9966/
                target: 'http://localhost:9966/',
                // target: 'http://192.168.98.217:9966',
                // target: 'http://192.168.56.1:8080',
                changeOrigin: true,
                rewrite: (path) => path.replace(/^\/api/, '') // 需要重写 '/api/login' 路径 => '/login'
            }
        }
    },
    assetsInclude: ['**/*.html']
})
